DROP DATABASE IF EXISTS ucode_race01_ayevtushen;

CREATE DATABASE ucode_race01_ayevtushen;

USE ucode_race01_ayevtushen;

DROP USER IF EXISTS 'ayevtushen' @'localhost';
CREATE USER 'ayevtushen' @'localhost' IDENTIFIED BY 'securepass';
GRANT ALL ON ucode_race01_ayevtushen.* TO 'ayevtushen' @'localhost';
