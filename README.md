# Installing

## Frontend

```shell
# ./fullstack-race01

cd frontend
yarn
```

## Backend

```shell
# ./fullstack-race01

cd backend

yarn

yarn setup
```

## For developing

```shell
# ./fullstack-race01

npm run prepare
```

# Starting for development

## Frontend

```shell
# ./fullstack-race01

cd frontend
yarn dev
```

## Backend

```shell
# ./fullstack-race01

cd backend
yarn dev
```
