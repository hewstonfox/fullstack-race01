import Game from './game/Game.js'
import Player from './game/player/Player.js'

const playerOne = new Player('p1', '123')
const playerTwo = new Player('p2', '456')

const game = new Game(playerOne, playerTwo, 'test')

game.playerReady(playerOne.login)
game.playerReady(playerTwo.login)

console.log(game.startGame())

// console.log(game.getGameSlice(playerOne.login))
// console.log(game.getGameSlice(playerTwo.login))

// console.log(game.playerOne.cards[0].isBlocked())

const playerOneCardId = game.playerOne.cards[0].id
const playerTwoCardId = game.playerTwo.cards[0].id

game.putCardOnTable(playerOne.login, playerOneCardId)

game.putCardOnTable(playerTwo.login, playerTwoCardId)

game.cardAttack(playerOne.login, playerOneCardId, playerTwoCardId)

console.log(game.getGameSlice(playerOne.login))

process.exit()
