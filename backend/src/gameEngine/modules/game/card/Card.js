export default class Card {
  isAlive = true
  blockedToMove = 1

  constructor(card) {
    this.id = card.id
    this.idx = card.idx
    this.mask = card.mask
    this.rarity = card.rarity
    this.hp = card.hp
    this.damage = card.damage
    this.cost = card.cost
    this.type = card.type
    this.name = card.name
    this.lvl = card.lvl
  }

  blockOn(currentMove, count) {
    this.blockedToMove = currentMove + count
  }

  takeDamage(damage) {
    this.hp = this.hp - damage
    if (this.hp <= 0) {
      this.isAlive = false
    }
  }

  isBlocked(currentMove) {
    return this.blockedToMove > currentMove
  }
}
