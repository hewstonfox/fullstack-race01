export default class Player {
  #connectionIds = []
  #cards = []
  #hp = 20
  mana = 0

  constructor(login, connectionId, avatarId) {
    this.login = login
    this.avatarID = avatarId ?? null

    if (Array.isArray(connectionId)) {
      connectionId.forEach(el => {
        this.#connectionIds.push(el)
      })
    } else {
      this.#connectionIds.push(connectionId)
    }
  }

  addConnection(connectionId) {
    this.#connectionIds.push(connectionId)
  }

  takeDamage(damage) {
    this.#hp = this.#hp - damage
  }

  giveCard(cardId) {
    if (!cardId) return null

    const filteredCards = this.#cards.filter(card => card.id === cardId)
    const card = filteredCards.length > 0 ? filteredCards[0] : null

    if (!card) {
      return false
    }

    this.#cards = this.#cards.filter(card => card.id !== cardId)
    return card
  }

  getCardsOnlyId() {
    return this.#cards.map(el => {
      return {
        id: el.id,
      }
    })
  }

  takeCard(card) {
    if (card) this.#cards.push(card)
  }

  getConnectionIds() {
    if (this.#connectionIds.length === 1) {
      return this.#connectionIds[0]
    }
    return this.#connectionIds
  }

  get cards() {
    return this.#cards
  }

  get stats() {
    return {
      login: this.login,
      hp: this.#hp,
      mana: this.mana,
      avatarID: this.avatarID,
    }
  }
}
