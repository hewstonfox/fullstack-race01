class Table {
  constructor(playerOne, playerTwo) {
    this.playerOne = playerOne
    this.playerTwo = playerTwo

    this.playerOneCards = []
    this.playerTwoCards = []
  }

  corpseCollector() {
    this.playerOneCards = this.playerOneCards.filter(card => card.hp > 0)
    this.playerTwoCards = this.playerTwoCards.filter(card => card.hp > 0)
  }

  getCardById(login, cardId) {
    const cards =
      login === this.playerOne.login ? this.playerOneCards : this.playerTwoCards

    const filtered = cards.filter(el => el.id === cardId)
    if (filtered.length > 0) return filtered[0]
    return null
  }

  patchCard(login, card) {
    let cards =
      login === this.playerOne.login ? this.playerOneCards : this.playerTwoCards

    cards = cards.map(el => {
      if (card.id === el.id) {
        el = card
      }
      return el
    })
  }

  addPlayerCard(login, card) {
    if (this.playerOne.login === login) {
      this.playerOneCards.push(card)
      return true
    }

    if (this.playerTwo.login === login) {
      this.playerTwoCards.push(card)
      return true
    }

    return false
  }

  getTableCards(login) {
    return login === this.playerOne.login
      ? this.playerOneCards
      : this.playerTwoCards
  }
}

export { Table }
