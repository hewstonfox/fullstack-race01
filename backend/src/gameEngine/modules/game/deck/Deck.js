import globalDeck from './globalDeck.js'
import { getRandomInt } from '../helpers.js'

class Deck {
  constructor() {
    this.deck = globalDeck
  }

  getRandomCardByLvl(lvl) {
    const cardsMatchLvl = this.deck.filter(el => el.lvl === lvl)
    if (cardsMatchLvl.length < 1) {
      return null
    }
    const randomIndex = getRandomInt(cardsMatchLvl.length - 1)
    const card = cardsMatchLvl[randomIndex]
    this.deck = this.deck.filter(el => el.id !== card.id)

    return card
  }

  giveOneCardByLevel(lvl) {}

  get length() {
    return this.deck.length
  }
}

export { Deck }
