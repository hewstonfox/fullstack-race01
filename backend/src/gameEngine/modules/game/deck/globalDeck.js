import fs from 'fs'
import pathUtils from '../../../../helpers/pathUtils.js'
import Card from '../card/Card.js'

const globalCards = JSON.parse(
  fs.readFileSync(pathUtils.localResolve(import.meta.url, 'cards.json'), 'utf8')
)

const globalDeck = globalCards.map(card => new Card(card))

export default globalDeck
