import { Deck } from './deck/Deck.js'
import Player from './player/Player.js'
import { Table } from './table/Table.js'

export default class Game {
  #isGameRunning = false
  #isFirstMoving = true
  #timeToNext = 30
  #currentMove = 1
  #readyPlayers = new Set()
  #nextMoveController = null
  #deck = new Deck()
  #table = null

  constructor(playerOne, playerTwo, room) {
    this.room = room

    this.playerOne = new Player(
      playerOne.login,
      playerOne.getConnectionIds(),
      playerOne.avatarID
    )
    this.playerTwo = new Player(
      playerTwo.login,
      playerTwo.getConnectionIds,
      playerTwo.avatarID
    )

    this.#table = new Table(this.playerOne, this.playerTwo)
  }

  // moves
  putCardOnTable(login, cardId) {
    const player =
      login === this.playerOne.login ? this.playerOne : this.playerTwo

    // TODO uncomment before push
    if (!this.isPlayerMoving(player.login)) {
      return false
    }

    const card = player.giveCard(cardId)
    if (!card) {
      return false
    }

    if (card.cost > player.mana) {
      player.takeCard(card)
      return false
    }

    player.mana = player.mana - card.cost
    card.blockedToMove = this.#currentMove + 1
    return this.#table.addPlayerCard(player.login, card)
  }

  cardAttack(login, from, to) {
    console.log('started', login, from, to)
    const me = login === this.playerOne.login ? this.playerOne : this.playerTwo
    const opponent =
      login !== this.playerOne.login ? this.playerOne : this.playerTwo

    if (!this.isPlayerMoving(me.login)) {
      console.log('player not moving')
      return false
    }

    const attackingCard = this.#table.getCardById(me.login, from)
    if (!attackingCard || attackingCard.isBlocked(this.#currentMove)) {
      if (!attackingCard) console.log('!attackingCard.isBlocked')
      else console.log('!attackingCard.isBlocked')

      return false
    }

    if (to === opponent.login) {
      opponent.takeDamage(attackingCard.damage)
      attackingCard.blockOn(this.#currentMove, 1)
      console.log('opponent take damage')
      return true
    } else {
      to = parseInt(to)
    }

    const defensiveCard = this.#table.getCardById(opponent.login, to)
    if (!defensiveCard) {
      console.log('!defencive card')
      return false
    }

    defensiveCard.takeDamage(attackingCard.damage)
    attackingCard.blockOn(this.#currentMove, 1)

    if (defensiveCard.isAlive) {
      attackingCard.takeDamage(defensiveCard.damage)
    }

    this.#table.patchCard(opponent.login, defensiveCard)
    this.#table.patchCard(me.login, attackingCard)

    this.#table.corpseCollector()
    console.log('end battle')
    return true
  }

  // game
  startGame() {
    if (this.#readyPlayers.size === 2) {
      this.#isGameRunning = true
      this.giveStartCards()
      this.#nextMoveController = this.setNextMoveTimer()

      return true
    }
  }

  giveStartCards() {
    this.giveOneCardByLvl(this.playerOne, this.#currentMove)
    this.giveOneCardByLvl(this.playerTwo, this.#currentMove)
  }

  giveOneCardByLvl(player, lvl) {
    let card = this.#deck.getRandomCardByLvl(lvl)
    if (card) player.takeCard(card)
  }

  changeNextMove() {
    this.#isFirstMoving = !this.#isFirstMoving
    this.#timeToNext = 30
    this.#currentMove += 1
    if (this.#isFirstMoving) {
      this.playerOne.mana += 1
    } else {
      this.playerTwo.mana += 1
    }
  }

  setNextMoveTimer() {
    return setInterval(() => {
      this.#timeToNext -= 1
      if (this.#timeToNext <= 0) {
        this.changeNextMove()
      }
    }, 1000)
  }

  isPlayerMoving(playerLogin) {
    return (
      (playerLogin === this.playerOne.login && this.#isFirstMoving) ||
      (playerLogin === this.playerTwo.login && !this.#isFirstMoving)
    )
  }

  // tech stuff
  restorePlayerId(playerLogin, newId) {
    if (playerLogin === this.playerOne.login) {
      this.playerOne.addConnection(newId)
    }
    if (playerLogin === this.playerTwo.login) {
      this.playerTwo.addConnection(newId)
    }
  }

  playerReady(playerLogin) {
    if (
      playerLogin === this.playerOne.login ||
      playerLogin === this.playerTwo.login
    ) {
      this.#readyPlayers.add(playerLogin)
      return true
    }
    return false
  }

  get isGameRunning() {
    return this.#isGameRunning
  }

  get readyPlayers() {
    return this.#readyPlayers
  }

  endTurn(playerLogin) {
    if (!this.isPlayerMoving(playerLogin)) return false
    this.changeNextMove()
    return true
  }

  getGameSlice(playerLogin) {
    const me =
      this.playerOne.login === playerLogin ? this.playerOne : this.playerTwo
    const opponent =
      this.playerOne.login !== playerLogin ? this.playerOne : this.playerTwo

    if (!me || !opponent) return null

    return {
      started: this.#isGameRunning,
      deck: this.#deck.length,
      myBoard: this.#table.getTableCards(me.login),
      opponentBoard: this.#table.getTableCards(opponent.login),
      myCards: me.cards,
      opponentCards: opponent.getCardsOnlyId(),
      me: me.stats,
      opponent: opponent.stats,
      isMyTurn: this.isPlayerMoving(playerLogin),
      currentMove: this.#currentMove,
      timeToNext: this.#timeToNext,
    }
  }
}
