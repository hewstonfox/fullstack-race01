const queue = []
const gameStorage = []

class QueueManager {
  #queue = []

  insert(data) {
    if (data) {
      this.#queue.push(data)
      console.log(this.#queue)
    }
  }

  spliceTwo() {
    if (this.length >= 2) {
      return this.#queue.splice(0, 2)
    }
  }

  findUserInQueue(login) {
    for (let user of this.#queue) if (user.login === login) return user
  }

  removeUserFromQueue(login) {
    this.#queue = this.#queue.filter(player => player.login !== login)
  }

  updatePlayer(login, newId) {
    for (let player of this.#queue) {
      if (player.login === login) {
        player.addConnection(newId)
      }
    }
  }

  get length() {
    return this.#queue.length
  }
}

const queueManager = new QueueManager()

export { queueManager }
