class StorageManager {
  #storage = []

  addGame(game) {
    if (!game) {
      return
    }
    this.#storage.push(game)
    return game
  }

  getGame(userLogin) {
    const game = this.#storage.filter(
      game =>
        game.playerOne.login === userLogin || game.playerTwo.login === userLogin
    )
    if (game.length >= 1) return game[0]
  }

  deleteGame(game) {
    if (!game) return
    this.#storage = this.#storage.filter(
      existGame =>
        existGame.playerOne.login !== game.playerOne.login ||
        existGame.playerTwo.login !== game.playerTwo.login
    )
  }

  get length() {
    return this.#storage.length
  }
}

const storageManager = new StorageManager()

export { storageManager }
