import fs from 'fs'
import pathUtils from '../helpers/pathUtils.js'
import { Sequelize } from 'sequelize'
import user from './models/User.js'

const config = JSON.parse(
  fs.readFileSync(
    pathUtils.localResolve(import.meta.url, 'config.json'),
    'utf8'
  )
)

export const sequelize = new Sequelize({
  ...config,
  ...(process.platform === 'darwin' && {
    dialectOptions: {
      socketPath: '/tmp/mysql.sock',
    },
  }),
})

try {
  await sequelize.authenticate()
} catch (error) {
  console.error('Unable to connect to the database:', error)
  process.exit(1)
}

for (const model of [user]) model(sequelize)

export default sequelize.models
