import { DataTypes } from 'sequelize'

import crypto from 'crypto'

export default sequelize => {
  sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      login: {
        type: DataTypes.STRING(30),
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING(130),
        allowNull: false,
        set(password) {
          this.setDataValue(
            'password',
            crypto
              .pbkdf2Sync(
                password,
                new TextEncoder().encode(process.env.SALT),
                1000,
                64,
                'sha512'
              )
              .toString('hex')
          )
        },
      },
      status: {
        type: DataTypes.ENUM('playing', 'searching', 'idle'),
        defaultValue: 'idle',
      },
      avatarID: {
        type: DataTypes.STRING(50),
      },
    },
    {
      sequelize,
      modelName: 'Users',
    }
  )
}
