import db from '../index.js'

import authInit from './auth.js'
import userInit from './user.js'

const Auth = authInit(db)
const User = userInit(db)

export { Auth, User }
