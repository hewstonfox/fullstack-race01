const insertUser = db => (login, password) => {
  return db.User.create({ login: login, password: password })
}

const buildUser = db => (login, password) =>
  db.User.build({ login: login, password: password })

const getUserByLogin = db => login => db.User.findOne({ where: { login } })

const getUserById = db => id => db.User.findOne({ where: { id } })

const pathUser = db => async (id, status, avatarId) => {
  const user = await db.User.findOne({ where: { id } })
  if (!user) return false

  user.status = status ? status : user.status
  user.avatarID = avatarId ? avatarId : user.avatarID

  try {
    await user.save({ fields: ['status', 'avatarID'] })
    return user
  } catch (e) {
    if (!e.message.startsWith('Data truncated')) console.log(e.message)
    return false
  }
}

const findUserByLoginAndPassword = db => async (login, password) => {
  const incomeUser = await db.User.build({ login, password })
  return await db.User.findOne({
    where: { login: incomeUser.login, password: incomeUser.password },
  })
}

export default db => ({
  insertUser: insertUser(db),
  buildUser: buildUser(db),
  getUserByLogin: getUserByLogin(db),
  findUserByLoginAndPassword: findUserByLoginAndPassword(db),
  getUserById: getUserById(db),
  pathUser: pathUser(db),
})
