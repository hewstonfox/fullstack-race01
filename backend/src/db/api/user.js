const changeUserStatus = db => async (login, status) => {
  const user = await db.User.findOne({ where: { login } })
  if (!user) return false

  user.status = status

  try {
    await user.save({ fields: ['status'] })
    return user
  } catch (e) {
    if (!e.message.startsWith('Data truncated')) console.log(e.message)
    return false
  }
}

export default db => ({
  changeUserStatus: changeUserStatus(db),
})
