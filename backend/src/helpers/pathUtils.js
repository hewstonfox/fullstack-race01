import path from 'path'
import url from 'url'

/**
 *
 * @param metaUrl {string} import.meta.url
 * @param crumbs {string}
 * @returns {string}
 */
export const localResolve = (metaUrl, ...crumbs) =>
  path.join(path.dirname(url.fileURLToPath(metaUrl)), ...crumbs)

export default {
  localResolve,
}
