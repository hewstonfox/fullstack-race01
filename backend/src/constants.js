export const isDev = process.env.MODE === 'development'
export const COOKIE_KEY = 'TYAN_BATTLE_USER_TOKEN'
export const USER_STATUS = {
  IDLE: 'idle',
  PLAYING: 'playing',
  SEARCHING: 'searching',
}
export const GAME_EVENT_TYPES = {
  GAME: 'game',
}
