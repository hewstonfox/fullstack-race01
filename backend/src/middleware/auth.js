import jwt from 'jsonwebtoken'
import { COOKIE_KEY } from '../constants.js'

const AuthMiddleware = () => async (ctx, next) => {
  if (ctx.path.startsWith('/auth')) return await next()

  const token = ctx.cookies.get(COOKIE_KEY)

  if (!token) {
    return (ctx.status = 401)
  }

  try {
    ctx.user = jwt.verify(token, process.env.TOKEN_KEY)
    return await next()
  } catch (e) {
    ctx.cookies.set(COOKIE_KEY, '')
    ctx.status = 401
  }
}

export default AuthMiddleware
