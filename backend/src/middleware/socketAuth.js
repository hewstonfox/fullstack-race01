import jwt from 'jsonwebtoken'

function AuthSocketMiddleware(socket, next) {
  const token =
    socket?.handshake?.auth?.token || socket?.handshake?.headers?.token
  if (token) {
    jwt.verify(token, process.env.TOKEN_KEY, (err, decoded) => {
      if (err) return next(new Error('Authentication error'))
      socket.login = decoded?.login
      next()
    })
  } else {
    next(new Error('Authentication error'))
  }
}

export default AuthSocketMiddleware
