import { io } from '../server.js'
import {
  findGame,
  gameJoin,
  gameReady,
  restoreGame,
  endTurn,
  putCard,
  attackCard,
  removeUserFromQueue,
} from './gameHandler.js'

export default async socket => {
  // restore prev game
  await restoreGame(io, socket)

  // refresh user data in queue
  // refreshUserInQueue(io, socket)

  // events
  socket.on('game:ready', gameReady(io))

  socket.on('game:join', gameJoin(io))

  socket.on('game:search', findGame(io))

  socket.on('endTurn', endTurn(io))

  socket.on('game:card:put', putCard(io))

  socket.on('game:card:attack', attackCard(io))

  socket.on('disconnect', async () => {
    await removeUserFromQueue(socket)
  })
}
