const buildPersonalRoom = name => `personal:${name}`

const genGameRoomName = (playerOne, playerTwo, id) =>
  `game:${playerOne}:${playerTwo}:${id}`

export { buildPersonalRoom, genGameRoomName }
