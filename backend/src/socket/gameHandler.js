import { queueManager } from '../gameEngine/gameStorageManager/queueManager.js'
import Game from '../gameEngine/modules/game/Game.js'
import { Auth, User } from '../db/api/api.js'
import { storageManager } from '../gameEngine/gameStorageManager/storageManager.js'
import { genGameRoomName, buildPersonalRoom } from './helpers/buildRoomName.js'
import Player from '../gameEngine/modules/game/player/Player.js'
import { USER_STATUS, GAME_EVENT_TYPES } from '../constants.js'

const timeToDelete = 15 * 1000

const findGame = io =>
  async function (task) {
    const socket = this

    const user = (await Auth.getUserByLogin(socket.login))?.dataValues

    if (!user || user.status !== USER_STATUS.IDLE) {
      return
    }

    const player = queueManager.findUserInQueue(socket.login)

    // add player to queue
    if (!player) {
      const player = new Player(socket.login, socket.id, user.avatarID)
      queueManager.insert(player)

      const patchStatus = await Auth.pathUser(
        user.id,
        USER_STATUS.SEARCHING,
        user.avatarID
      )

      if (patchStatus) {
        io.to(player.getConnectionIds()).emit('search:added', 'added')
      } else {
        io.to(player.getConnectionIds()).emit('develop', 'cannot patch user')
      }
    }

    // Create game
    if (queueManager.length >= 2) {
      const [playerOne, playerTwo] = queueManager.spliceTwo()
      const gameRoom = genGameRoomName(
        playerOne.login,
        playerTwo.login,
        queueManager.length
      )

      const game = new Game(playerOne, playerTwo, gameRoom)
      storageManager.addGame(game)

      setTimeout(() => {
        if (!game.isGameRunning) {
          storageManager.deleteGame(game)
          io.to(gameRoom).emit(
            'game:room:deleted',
            'game room has been deleted'
          )
        }
      }, timeToDelete)

      const userOne = (await Auth.getUserByLogin(game.playerOne.login))
        ?.dataValues
      const userTwo = (await Auth.getUserByLogin(game.playerTwo.login))
        ?.dataValues

      if (!userOne || !userTwo) {
        return
      }

      await Auth.pathUser(userOne.id, USER_STATUS.PLAYING, userOne.avatarID)
      await Auth.pathUser(userTwo.id, USER_STATUS.PLAYING, userTwo.avatarID)

      io.to(playerOne.getConnectionIds())
        .to(playerTwo.getConnectionIds())
        .emit('game:room:created', gameRoom)
    }
  }

const gameReady = io =>
  async function () {
    const socket = this

    const game = storageManager.getGame(socket.login)
    if (!game) {
      await User.changeUserStatus(socket.login, USER_STATUS.IDLE)
      io.to(socket.id).emit('game:join:fail', 'failed')
      return
    }

    if (game.isGameRunning) {
      socket.emit('game:update', {
        type: GAME_EVENT_TYPES.GAME,
        payload: game.getGameSlice(game.playerOne.login),
      })
      return
    }

    const ready = game.playerReady(socket.login)
    if (!ready) return

    if (game.readyPlayers.size === 2) {
      game.startGame()

      // emit game slice to each player
      io.to(game.playerOne.getConnectionIds()).emit('game:update', {
        type: GAME_EVENT_TYPES.GAME,
        payload: game.getGameSlice(game.playerOne.login),
      })

      io.to(game.playerTwo.getConnectionIds()).emit('game:update', {
        type: GAME_EVENT_TYPES.GAME,
        payload: game.getGameSlice(game.playerTwo.login),
      })
    }
  }

// add to room + send game instance only to socket
const gameJoin = io =>
  async function () {
    const socket = this

    const game = storageManager.getGame(socket.login)

    if (!game) {
      await User.changeUserStatus(socket.login, USER_STATUS.IDLE)
      io.to(socket.id).emit('game:join:fail', 'failed')
      return
    }

    socket.join(game.room)

    socket.emit('game:update', {
      type: GAME_EVENT_TYPES.GAME,
      payload: game.getGameSlice(game.playerOne.login),
    })
  }

async function restoreGame(io, socket) {
  const game = storageManager.getGame(socket.login)
  if (!game) {
    socket.hasGame = false

    const user = (await Auth.getUserByLogin(socket.login))?.dataValues
    if (!user) {
      return
    }
    await Auth.pathUser(user.id, USER_STATUS.IDLE, user.avatarID)

    return
  }

  game.restorePlayerId(socket.login, socket.id)
}

function refreshUserInQueue(io, socket) {
  if (queueManager.findUserInQueue(socket.login)) {
    queueManager.updatePlayer(socket.login, socket.id)
  }
}

async function removeUserFromQueue(socket) {
  queueManager.removeUserFromQueue(socket.login)

  const user = (await Auth.getUserByLogin(socket.login))?.dataValues

  if (!user || user.status === USER_STATUS.PLAYING) {
    return
  }

  await Auth.pathUser(user.id, USER_STATUS.IDLE, user.avatarID)
}

const endTurn = io =>
  function endTurn() {
    const socket = this

    const game = storageManager.getGame(socket.login)

    if (!game) {
      return
    }

    const turnEnded = game.endTurn(socket.login)

    if (turnEnded) {
      io.to(game.playerOne.getConnectionIds()).emit('game:update', {
        type: GAME_EVENT_TYPES.GAME,
        payload: game.getGameSlice(game.playerOne.login),
      })

      io.to(game.playerTwo.getConnectionIds()).emit('game:update', {
        type: GAME_EVENT_TYPES.GAME,
        payload: game.getGameSlice(game.playerTwo.login),
      })
    }
  }

const putCard = io =>
  function putCard(cardId) {
    const socket = this

    const game = storageManager.getGame(socket.login)
    if (!game) {
      return
    }

    const isCardPlaced = game.putCardOnTable(socket.login, cardId)
    if (isCardPlaced) {
      io.to(game.playerOne.getConnectionIds()).emit('game:update', {
        type: GAME_EVENT_TYPES.GAME,
        payload: game.getGameSlice(game.playerOne.login),
      })

      io.to(game.playerTwo.getConnectionIds()).emit('game:update', {
        type: GAME_EVENT_TYPES.GAME,
        payload: game.getGameSlice(game.playerTwo.login),
      })
    }
  }

const attackCard = io =>
  function (task) {
    const socket = this

    if (!task.to || !task.from) {
      return
    }

    const game = storageManager.getGame(socket.login)
    if (!game) {
      return
    }

    const isAttacked = game.cardAttack(socket.login, task.from, task.to)

    if (isAttacked) {
      io.to(game.playerOne.getConnectionIds()).emit('game:update', {
        type: GAME_EVENT_TYPES.GAME,
        payload: game.getGameSlice(game.playerOne.login),
      })

      io.to(game.playerTwo.getConnectionIds()).emit('game:update', {
        type: GAME_EVENT_TYPES.GAME,
        payload: game.getGameSlice(game.playerTwo.login),
      })
    }
  }

export {
  findGame,
  gameJoin,
  gameReady,
  restoreGame,
  refreshUserInQueue,
  removeUserFromQueue,
  endTurn,
  putCard,
  attackCard,
}
