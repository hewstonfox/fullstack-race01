import Koa from 'koa'
import { createServer } from 'http'
import { Server } from 'socket.io'
import Formidable from 'koa2-formidable'
import BodyParser from 'koa-bodyparser'
import Logger from 'koa-logger'
import Json from 'koa-json'
import Cors from '@koa/cors'
import router from './routes/mainRouter.js'
import { isDev } from './constants.js'
import AuthMiddleware from './middleware/auth.js'

import AuthSocketMiddleware from './middleware/socketAuth.js'
import onConnection from './socket/onConnection.js'

const app = new Koa()
  .use(Logger((_, args) => isDev && console.log(...args)))
  .use(
    Cors({
      credentials: true,
    })
  )
  .use(AuthMiddleware())
  .use(Formidable())
  .use(BodyParser())
  .use(router.routes())
  .use(router.allowedMethods())
  .use(Json())

const httpServer = createServer(app.callback())
const io = new Server(httpServer, {
  cookie: true,
  cors: {
    origin: true,
  },
})

io.use(AuthSocketMiddleware)

io.on('connection', await onConnection)

export { httpServer, io }
