import jwt from 'jsonwebtoken'

import { COOKIE_KEY } from '../constants.js'
import { Auth } from '../db/api/api.js'

const authController = {}

authController.logout = async ctx => {
  ctx.cookies.set(COOKIE_KEY, '')
  ctx.user = null
  ctx.status = 200
}

authController.login = async ctx => {
  const { login, password } = ctx.request.body
  if (!login || !password) return (ctx.status = 400)

  return Auth.findUserByLoginAndPassword(login, password)
    .then(user => {
      const token = jwt.sign({ login: user.login }, process.env.TOKEN_KEY, {
        expiresIn: '12h',
      })
      ctx.cookies.set(COOKIE_KEY, token, { httpOnly: false, sameSite: 'lax' })
      ctx.status = 200
    })
    .catch(e => {
      ctx.status = 401
      ctx.body = { message: 'Invalid email or password' }
    })
}

authController.register = async ctx => {
  const { login, password } = ctx.request.body
  if (!login || !password) return (ctx.status = 400)

  return Auth.insertUser(login, password)
    .then(_ => {
      const token = jwt.sign({ login }, process.env.TOKEN_KEY, {
        expiresIn: '12h',
      })
      ctx.status = 200
      ctx.cookies.set(COOKIE_KEY, token, { httpOnly: false, sameSite: 'lax' })
    })
    .catch(e => {
      const message = e.message.startsWith('Duplicate entry')
        ? 'User already registered'
        : e.message
      ctx.status = 409
      ctx.body = { message }
    })
}

export default authController
