import { Auth } from '../db/api/api.js'
import Game from '../gameEngine/modules/game/Game.js'
import { queueManager } from '../gameEngine/gameStorageManager/queueManager.js'
import { storageManager } from '../gameEngine/gameStorageManager/storageManager.js'

const gameController = {}

gameController.findGame = async ctx => {
  const user = await Auth.getUserByLogin(ctx.user.login)
  if (!user) return (ctx.status = 401)

  const userData = user.dataValues
  if (userData.status === 'playing') {
    return ctx.redirect('/game')
  }

  const position = queueManager.findUserInQueue(userData.id)

  if (!position) {
    await queueManager.insert(userData.id)
  } else {
    ctx.body = 'already in queue'
    return (ctx.status = 200)
  }

  if (queueManager.length >= 2) {
    const players = queueManager.spliceTwo()

    for (let player of players) {
      await Auth.pathUser(player, 'playing', null)
    }

    const game = await new Game(players[0], players[1])

    if (!game) {
      ctx.status = 400
      ctx.body = 'Something went wrong'
      return
    }

    storageManager.push(game)
  }

  ctx.status = 200
}

gameController.getGame = async ctx => {
  const user = await Auth.getUserByLogin(ctx?.user?.login)
  if (!user) return (ctx.status = 401)

  const game = await storageManager.getMyGame(user.id)

  if (game.length === 1) {
    ctx.body = game[0].getGameSlice(user.id)
    ctx.status = 200
    return
  } else {
    await Auth.pathUser(user.id, 'idle', null)
    ctx.body = 'Cannot find game, move user state to idle'
    ctx.status = 200
  }
}

gameController.endMove = async ctx => {
  const user = await Auth.getUserByLogin(ctx?.user?.login)
  if (!user) return (ctx.status = 401)

  const game = await storageManager.getMyGame(user.id)
  if (game[0]?.changeNextMove(user.id)) {
    ctx.status = 200
  } else {
    ctx.status = 403
  }
}

export default gameController
