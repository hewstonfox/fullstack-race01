import path from 'path'
import fs from 'fs'
import crypto from 'crypto'
import mime from 'mime-types'

export class FileStorage {
  constructor(dirRoute) {
    this.dirRoute = dirRoute
    this.path = path.join(process.cwd(), dirRoute)
    if (!fs.existsSync(this.path)) fs.mkdirSync(this.path, { recursive: true })
  }

  async generateNameByFileContent(filePath) {
    return crypto
      .createHash('md5')
      .update(await fs.promises.readFile(filePath))
      .digest('hex')
  }

  async saveFile(file) {
    const name = await this.generateNameByFileContent(file.path)
    const ext = path.extname(file.name)

    let attr = ''
    let counter = 0
    let fileData = null
    let dupPath
    while (
      await fs.promises
        .access((dupPath = path.join(this.path, `${name}${attr}${ext}`)))
        .then(() => true)
        .catch(() => false)
    ) {
      if (!fileData) fileData = await fs.promises.readFile(file.path)
      if (Buffer.compare(fileData, await fs.promises.readFile(dupPath)) === 0)
        return path.basename(dupPath)
      attr = `_${counter}`
      counter++
    }

    await fs.promises.copyFile(file.path, dupPath)
    return path.basename(dupPath)
  }

  getFileStream(name) {
    const filePath = path.join(this.path, name)
    return [fs.createReadStream(filePath), mime.lookup(filePath)]
  }
}

export default class StorageController {
  static rootDir = 'storage'
  static #storages = {}
  static #namedStorages = {}

  static getOrCreateStorage(dirRoute, name) {
    const rootedDirRoute = path.join(this.rootDir, dirRoute)
    if (
      !(rootedDirRoute in this.#storages) &&
      (!name || !(name in this.#namedStorages))
    ) {
      const storage = new FileStorage(rootedDirRoute)
      this.#storages[rootedDirRoute] = storage
      if (name) this.#namedStorages[name] = storage
    }
    return this.#storages[rootedDirRoute] ?? this.#namedStorages[name]
  }

  static getStorageByName(name) {
    return this.#namedStorages[name]
  }
}

export const AVATAR_STORAGE_NAME = 'avatars'
