import StorageController, { AVATAR_STORAGE_NAME } from './fileController.js'

import { Auth } from '../db/api/api.js'

const UserController = {}

UserController.getUser = async ctx => {
  const login = ctx.user.login
  if (!login) return 400

  const user = await Auth.getUserByLogin(login)
  if (!user) return (ctx.status = 404)

  const userData = user.dataValues
  delete userData.hash
  ctx.body = userData
  ctx.status = 200
}

UserController.patchUser = async ctx => {
  const { status, avatarId } = ctx.request.body

  const user = await Auth.getUserByLogin(ctx.user.login)
  if (!user) return (ctx.status = 404)

  const res = await Auth.pathUser(user.id, status, avatarId)

  if (!res) {
    return (ctx.status = 400)
  }

  ctx.status = 200
}

UserController.getAvatar = async login => {
  const user = await Auth.getUserByLogin(login)
  if (!user.avatarID) return null
  return StorageController.getStorageByName(AVATAR_STORAGE_NAME).getFileStream(
    user.avatarID
  )
}

UserController.setAvatar = async (login, file) => {
  const user = await Auth.getUserByLogin(login)

  user.avatarID = await StorageController.getStorageByName(
    AVATAR_STORAGE_NAME
  ).saveFile(file)

  user.save({ fields: ['avatarID'] })

  return user.avatarID
}

UserController.changePassword = async (login, curentPassword, newPassword) => {
  const user = await Auth.getUserByLogin(login)
  if (!user) return
  const incomeUser = Auth.buildUser(login, curentPassword)

  if (incomeUser.password === user.password) {
    user.password = newPassword
    user.save({ fields: ['password'] })
  }
}

export default UserController
