import Router from 'koa-router'

import authController from '../controllers/authController.js'
const authRouter = new Router()

authRouter
  .post('/logout', authController.logout)
  .post('/login', authController.login)
  .post('/register', authController.register)

export default authRouter
