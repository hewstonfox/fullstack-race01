import Router from 'koa-router'
import authRouter from './authRouter.js'
import userRouter from './userRouter.js'
import gameRouter from './gameRouter.js'

const router = new Router()

router
  .use('/auth', authRouter.routes(), authRouter.allowedMethods())
  .use('/user', userRouter.routes(), userRouter.allowedMethods())
  .use('/game', gameRouter.routes(), gameRouter.allowedMethods())

export default router
