import Router from 'koa-router'

import gameController from '../controllers/gameController.js'
const gameRouter = new Router()

gameRouter.get('/', gameController.getGame)
gameRouter.get('/find', gameController.findGame)

gameRouter.post('/endmove', gameController.endMove)

export default gameRouter
