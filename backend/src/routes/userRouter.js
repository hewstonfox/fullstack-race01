import Router from 'koa-router'
import StorageController, {
  AVATAR_STORAGE_NAME,
} from '../controllers/fileController.js'

import userController from '../controllers/userController.js'

const userRouter = new Router()

userRouter
  .get('/', userController.getUser)
  .patch('/', userController.patchUser)
  .get('/avatar', async ctx => {
    const avatar = await userController.getAvatar(ctx.user.login)

    if (!avatar) {
      ctx.body = { message: 'Avatar is not found' }
      ctx.status = 404
      return
    }

    ;[ctx.body, ctx.type] = avatar
  })
  .post('/avatar', async ctx => {
    const file = ctx.request.files.avatar
    if (!file) {
      ctx.body = { message: 'Avatar is not provided' }
      ctx.status = 400
      return
    }

    ctx.body = { url: await userController.setAvatar(ctx.user.login, file) }
    ctx.status = 200
  })
  .patch('/password', async ctx => {
    const { currentPassword, newPassword } = ctx.request.body
    if (currentPassword === newPassword) {
      ctx.body = { message: 'Passwords match' }
      ctx.status = 400
      return
    }
    if (
      userController.changePassword(
        ctx.user.login,
        currentPassword,
        newPassword
      ) == null
    ) {
      ctx.body = { message: 'Oh shit, something went wrong' }
      ctx.status = 500
    }
    ctx.status = 200
    ctx.body = { message: 'Password changed' }
  })
  .get('/file/:id', async ctx => {
    const avatar = StorageController.getStorageByName(
      AVATAR_STORAGE_NAME
    ).getFileStream(ctx.params.id)

    if (!avatar) {
      ctx.body = { message: 'Avatar is not found' }
      ctx.status = 404
      return
    }

    ;[ctx.body, ctx.type] = avatar
  })

export default userRouter
