import 'dotenv/config'
import { httpServer } from './src/server.js'
import StorageController, {
  AVATAR_STORAGE_NAME,
} from './src/controllers/fileController.js'

httpServer.listen(process.env.PORT, () => {
  StorageController.getOrCreateStorage('avatars', AVATAR_STORAGE_NAME)
  console.log(`Server started at http://localhost:${process.env.PORT}`)
  console.log(`Using ${process.env.MODE} mode`)
})
