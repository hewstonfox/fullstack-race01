import { sequelize } from './src/db/index.js'

await sequelize.sync({ alter: true })

process.exit(0)
