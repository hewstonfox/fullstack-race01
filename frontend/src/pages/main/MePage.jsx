import Acryl from '../../components/Acryl/Acryl'
import { Link } from 'react-router-dom'
import Button from '../../components/Button'
import { logout } from '../../api/auth'
import { useQueryClient } from 'react-query'
import UserView from '../../components/UserView/UserView'
import { useEffect } from 'react'
import { useAudio } from '../../components/AudioContext/AudioContext'

const MePage = () => {
  const { audio, setAudio } = useAudio()
  const queryClient = useQueryClient()

  const logoutHandler = () => {
    logout().then(() => queryClient.invalidateQueries('user'))
  }

  useEffect(() => {
    if (audio) return
    setAudio(new Audio('/menu.mp3'))
  }, [audio])

  return (
    <Acryl
      className="paper"
      blendColor="rgba(253, 202, 243, 0.2)"
      style={{
        justifyContent: 'space-around',
      }}
    >
      <div className="flex-content">
        <div className="grid-content">
          <Button onClick={logoutHandler}>Logout</Button>
          <Link to="/">Main</Link>
        </div>
        <UserView />
      </div>
    </Acryl>
  )
}

export default MePage
