import Acryl from '../../components/Acryl/Acryl'
import { Link, useNavigate } from 'react-router-dom'
import Button from '../../components/Button'
import { useEffect, useLayoutEffect, useRef, useState } from 'react'
import { useUser } from '../../queries'
import { USER_STATUS } from '../../constants'
import { useSocket } from '../../components/SocketContext/SocketContext'
import { useQueryClient } from 'react-query'
import { useAudio } from '../../components/AudioContext/AudioContext'

const MainPage = () => {
  const { audio, setAudio } = useAudio()

  const queryClient = useQueryClient()
  const socketRef = useSocket()
  const navigate = useNavigate()

  const { data: user } = useUser()

  const [started, setStarted] = useState(false)
  const dotRef = useRef(null)

  const startHandler = () => {
    setStarted(true)
  }

  const stopHandler = () => {
    setStarted(false)
  }

  useEffect(() => {
    if (started) {
      const invalidateHandler = () => {
        queryClient.invalidateQueries('user')
      }
      const disconnectHandler = () => {
        setStarted(false)
      }

      socketRef.current.on('disconnect', disconnectHandler)
      socketRef.current.on('game:room:created', invalidateHandler)
      socketRef.current.on('search:added', invalidateHandler)

      socketRef.current.connect()
      socketRef.current.emit('game:search')

      return () => {
        socketRef.current.off('game:room:created', invalidateHandler)
        socketRef.current.off('disconnect', disconnectHandler)
        socketRef.current.on('search:added', invalidateHandler)
        socketRef.current.disconnect()
      }
    } else {
      socketRef.current.disconnect()
      const timeout = setTimeout(() => {
        queryClient.invalidateQueries('user')
      }, 1000)
      return () => {
        clearTimeout(timeout)
      }
    }
  }, [started])

  useEffect(() => {
    switch (user.status) {
      case USER_STATUS.SEARCHING:
        setStarted(true)
        return
      case USER_STATUS.IDLE:
        setStarted(false)
        return
      case USER_STATUS.PLAYING:
        navigate('/game')
        return
    }
  }, [user.status])

  useLayoutEffect(() => {
    if (!started) return
    let count = 0
    const elem = dotRef.current
    const defaultText = 'Waiting for opponent'
    elem.innerText = defaultText
    const interval = setInterval(() => {
      if (count >= 4) count = 0
      elem.innerText = defaultText + '.'.repeat(count)
      count++
    }, 500)
    return () => {
      clearInterval(interval)
      elem.innerText = defaultText
    }
  }, [started])

  useEffect(() => {
    if (audio) return
    setAudio(new Audio('/menu.mp3'))
  }, [audio])

  return (
    <>
      <Acryl className="paper" blendColor="#00ff000f">
        <div className="grid-content">
          {started ? (
            <Button onClick={stopHandler}>Stop</Button>
          ) : (
            <Button onClick={startHandler}>Start</Button>
          )}
          <Link
            to={started ? '/' : '/me'}
            className={started ? 'disabled' : ''}
          >
            Settings
          </Link>
        </div>
        <div
          className={`drawer ${started ? 'drawer--showed' : 'drawer--hidden'}`}
          ref={dotRef}
        />
      </Acryl>
    </>
  )
}

export default MainPage
