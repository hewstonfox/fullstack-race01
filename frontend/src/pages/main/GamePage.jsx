import { useNavigate } from 'react-router-dom'
import { useUser } from '../../queries'
import { useEffect } from 'react'
import { USER_STATUS } from '../../constants'
import GameTable from '../../components/GameTable/GameTable'

const GamePage = () => {
  const navigate = useNavigate()
  const { data } = useUser()

  useEffect(() => {
    if (data?.status === USER_STATUS.PLAYING) return
    navigate('/')
  }, [data])

  return <GameTable />
}

export default GamePage
