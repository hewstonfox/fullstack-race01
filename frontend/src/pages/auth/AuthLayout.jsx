import './auth.css'
import { Outlet } from 'react-router-dom'
import { useEffect, useState } from 'react'
import Acryl from '../../components/Acryl/Acryl'
import { useAudio } from '../../components/AudioContext/AudioContext'

const AuthLayout = () => {
  const { setAudio } = useAudio()

  const loginInputState = useState('')

  useEffect(() => {
    setAudio(null)
  }, [])

  return (
    <Acryl className="paper">
      <Outlet context={loginInputState} />
    </Acryl>
  )
}

export default AuthLayout
