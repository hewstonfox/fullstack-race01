import { Link, useOutletContext } from 'react-router-dom'
import { useState } from 'react'
import { login } from '../../api/auth'
import { useQueryClient } from 'react-query'
import Button from '../../components/Button'

const LoginPage = () => {
  const queryClient = useQueryClient()

  const [loginInput, setLoginInput] = useOutletContext()
  const [password, setPassword] = useState('')

  const [error, setError] = useState('')

  const submitHandler = e => {
    e.preventDefault()
    login({ login: loginInput, password })
      .then(() => queryClient.invalidateQueries('user'))
      .catch(e => setError(e.response?.data?.message ?? e.message))
  }

  return (
    <form onSubmit={submitHandler} className="flex-content">
      <h2>Authorization</h2>
      <input
        type="text"
        name="login"
        value={loginInput}
        onChange={e => setLoginInput(e.target.value)}
        placeholder="Login"
        required
      />
      <input
        type="password"
        name="password"
        value={password}
        onChange={e => setPassword(e.target.value)}
        placeholder="Password"
        required
      />
      <div>
        <Button submit>Login</Button>
        &nbsp;
        <span className="danger">{error}</span>
      </div>
      <Link to="/auth/registration">Register</Link>
    </form>
  )
}

export default LoginPage
