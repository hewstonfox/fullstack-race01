import * as React from 'react'
import { useState } from 'react'
import { Link, useOutletContext } from 'react-router-dom'
import { useQueryClient } from 'react-query'
import { register } from '../../api/auth'
import Button from '../../components/Button'

const RegisterPage = () => {
  const queryClient = useQueryClient()

  const [loginInput, setLoginInput] = useOutletContext()
  const [password, setPassword] = useState('')
  const [rPassword, setRPassword] = useState('')

  const [error, setError] = useState('')

  const submitHandler = e => {
    e.preventDefault()
    if (password !== rPassword) return setError('Passwords don`t match')
    register({ login: loginInput, password })
      .then(() => queryClient.invalidateQueries('user'))
      .catch(e => setError(e.response?.data?.message ?? e.message))
  }

  return (
    <form onSubmit={submitHandler} className="flex-content">
      <h2>Registration</h2>
      <input
        type="text"
        name="login"
        value={loginInput}
        onChange={e => setLoginInput(e.target.value)}
        placeholder="Login"
        required
      />
      <input
        type="password"
        name="login"
        value={password}
        onChange={e => setPassword(e.target.value)}
        placeholder="Password"
        required
      />
      <input
        type="password"
        name="rPassword"
        value={rPassword}
        onChange={e => setRPassword(e.target.value)}
        placeholder="Repeat password"
        required
      />
      <div>
        <Button submit>Register</Button>
        &nbsp;
        <span className="danger">{error}</span>
      </div>
      <Link to="/auth/login">Login</Link>
    </form>
  )
}

export default RegisterPage
