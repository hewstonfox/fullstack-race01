import React from 'react'

const AdoptiveText = ({ children, viewBox, svgProps, ...props }) => (
  <svg width="100%" height="100%" viewBox={viewBox} {...svgProps}>
    <foreignObject width="100%" height="100%" {...props}>
      {children}
    </foreignObject>
  </svg>
)

export default AdoptiveText
