import { useMemo } from 'react'

const Button = ({ onClick, onSubmit, className, submit, primary = true, secondary = false, ...buttonProps }) => {
  const btnClass = useMemo(() => {
    switch (true) {
      case primary:
        return 'primary'
      case secondary:
        return 'secondary'
    }
  }, [])

  return (
    <button
      type={onSubmit || submit ? 'submit' : 'button'}
      onSubmit={onSubmit}
      onClick={onClick}
      className={`button ${btnClass} ${className ?? ''}`}
      {...buttonProps}
    />
  )
}

export default Button
