import './imageChooser.css'
import { useRef } from 'react'
import { file2dataUrl } from '../../utils/file'

const ImageChooser = ({ image, setImage, placeholder, className, style }) => {
  const inputRef = useRef(null)

  const imgErrorHandler = e => (e.target.src = placeholder)
  const actorClickHandler = () => inputRef.current.click()
  const inputChangeHandler = async e => {
    setImage(await file2dataUrl(e.target.files[0]), e.target.files[0])
  }
  return (
    <div className={`image-chooser ${className ?? ''}`} style={style}>
      <input type="file" className="image-chooser--input" ref={inputRef} onChange={inputChangeHandler} />
      <img
        src={image}
        alt="avatar"
        onError={imgErrorHandler}
        className="image-chooser--inherit image-chooser--preview"
      />
      <div className="image-chooser--inherit image-chooser--actor" onClick={actorClickHandler} />
    </div>
  )
}

export default ImageChooser
