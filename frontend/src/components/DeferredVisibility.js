import { useEffect, useState } from 'react'

export const DeferredVisibility = ({ show, timeoutMs = 0, children }) => {
  const [deferredShow, setDeferredShow] = useState(show)

  useEffect(() => {
    const timeout = setTimeout(() => setDeferredShow(show), timeoutMs)
    return () => {
      clearTimeout(timeout)
      setDeferredShow(show)
    }
  }, [show, timeoutMs])

  return deferredShow ? children : null
}
