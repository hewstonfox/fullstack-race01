import { useUser } from '../../queries'
import ImageChooser from '../ImageChooser/ImageChooser'
import { setMyAvatar, updateUserPassword, USER_AVATAR_URL } from '../../api/user'
import { useRef, useState, useEffect } from 'react'
import Button from '../Button'
import avatarPlaceholder from '@/assets/avatarPlaceholder.jpg'
import './userView.css'

const UserView = () => {
  const { data: user } = useUser()

  const timeoutRef = useRef(null)
  const [justUpdatedAvatar, setJustUpdatedAvatar] = useState(false)
  const [newAvatar, setNewAvatar] = useState(null)
  const [error, setError] = useState('')

  const saveAvatar = () => {
    if (!newAvatar?.file) return
    setMyAvatar(newAvatar.file)
      .then(() => {
        setJustUpdatedAvatar(true)
        timeoutRef.current = setTimeout(() => {
          setJustUpdatedAvatar(false)
          timeoutRef.current = null
        }, 500)
      })
      .catch(e => setError(e.response?.data?.message ?? e.message))
  }

  useEffect(
    () => () => {
      if (timeoutRef.current) clearTimeout(timeoutRef.current)
    },
    []
  )

  const changePassword = e => {
    const formData = new FormData(e.target)
    const cPassword = formData.get('cPassword')
    const password = formData.get('password')
    const rPassword = formData.get('rPassword')

    if (!cPassword && !password) return

    let error = null
    if (!cPassword) error = 'Current password is required'
    if (!error && !password) error = 'New password can`t be empty'
    if (!error && password !== rPassword) error = 'Passwords not match'
    if (error) {
      setError(error)
      return
    }

    updateUserPassword(cPassword, password)
      .then(() => {
        e.target.reset()
        setError('')
      })
      .catch(e => setError(e.response?.data?.message ?? e.message))
  }

  const handleSave = e => {
    e.preventDefault()
    saveAvatar(e)
    changePassword(e)
  }

  return (
    <form className="user-view-container" onSubmit={handleSave}>
      <ImageChooser
        image={newAvatar?.url ?? USER_AVATAR_URL}
        placeholder={avatarPlaceholder}
        setImage={(url, file) => setNewAvatar({ url, file })}
        className={`user-view--avatar ${justUpdatedAvatar ? 'user-view--avatar--updated' : ''}`}
      />
      <div className="flex-content user-view-container--info">
        <div className="flex-content-row">
          <div className="user-view--login">{user.login}</div>
          <span className="danger">{error}&nbsp;</span>
        </div>
        <input type="password" placeholder="Current password" name="cPassword" />
      </div>
      <div className="flex-row-content user-view--passwords">
        <input type="password" placeholder="New password" name="password" />
        <input type="password" placeholder="Repeat new password" name="rPassword" />
      </div>
      <div className="user-view--controls">
        <Button type="submit">Save</Button>
      </div>
    </form>
  )
}

export default UserView
