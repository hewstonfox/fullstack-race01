import LoadingTyan from '@/assets/loading.gif'

const Loading = () => {
  return <img src={LoadingTyan} alt="loading" />
}

export default Loading
