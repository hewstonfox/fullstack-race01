import { useLayoutEffect, useState } from 'react'
import { useAntiAcrylicCursor } from './acrylHooks'
import './acryl.css'

const Acryl = ({
  children,
  background,
  blendColor,
  className,
  contentClassName,
  contentRef,
  allClassName,
  disableCursor,
}) => {
  const [computedBgImageStyle, setComputedBgImageStyle] = useState({})

  const [wrapperRef, cursorRef] = useAntiAcrylicCursor()

  useLayoutEffect(() => {
    if (background) {
      setComputedBgImageStyle({ background })
      return
    }
    let parent = wrapperRef.current.parentElement
    while (parent && getComputedStyle(parent).backgroundImage === 'none')
      parent = parent.parentElement

    if (parent)
      setComputedBgImageStyle({
        background: getComputedStyle(parent).background,
      })
  }, [background])

  return (
    <div
      className={`acrylic ${className ?? ''} ${allClassName ?? ''}`}
      ref={wrapperRef}
    >
      <div
        className={`acrylic-layer acrylic--blur ${allClassName ?? ''}`}
        style={computedBgImageStyle}
      />
      <div
        className={`acrylic-layer acrylic--blend ${allClassName ?? ''}`}
        style={{
          backgroundColor: blendColor,
        }}
      />
      <div className={`acrylic-layer acrylic--noise ${allClassName ?? ''}`} />
      {!disableCursor && (
        <div className="anti-acrylic-cursor" ref={cursorRef} />
      )}
      <div
        className={`acrylic--content ${contentClassName ?? ''} ${
          allClassName ?? ''
        }`}
        ref={contentRef}
      >
        {children}
      </div>
    </div>
  )
}

export default Acryl
