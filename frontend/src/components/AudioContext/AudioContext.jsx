import {
  createContext,
  useRef,
  useLayoutEffect,
  useContext,
  useState,
} from 'react'

export const AudioContext = createContext(null)

const VOLUME_KEY = 'TYAN_BATTLE_MUSIC_VOLUME'

export const AudioContextProvider = ({ children }) => {
  const [audio, setAudio] = useState(null)
  const prevAudioRef = useRef(null)
  const currentAudioRef = useRef(null)

  useLayoutEffect(() => {
    currentAudioRef.current = audio

    if (!currentAudioRef.current) {
      if (!prevAudioRef.current) return
      prevAudioRef.current.pause()
      return
    }

    currentAudioRef.current.currentTime = 0
    currentAudioRef.current.volume = 0.2

    if (prevAudioRef.current) {
      prevAudioRef.current.pause()
      currentAudioRef.current.volume = prevAudioRef.current.volume
    }
    const curVolume = +localStorage.getItem(VOLUME_KEY)
    if (!Number.isNaN(curVolume) && curVolume >= 0 && curVolume <= 1)
      currentAudioRef.current.volume = curVolume

    const audioEndHandler = () => {
      if (currentAudioRef.current !== audio) return
      currentAudioRef.current.currentTime = 0
      currentAudioRef.current.play()
    }
    currentAudioRef.current.addEventListener('ended', audioEndHandler, false)

    const subscribeAll = () => {
      window.addEventListener('mousemove', startAudioHandler)
      window.addEventListener('scroll', startAudioHandler)
      window.addEventListener('keydown', startAudioHandler)
      window.addEventListener('click', startAudioHandler)
      window.addEventListener('touchstart', startAudioHandler)
      window.addEventListener('wheel', startAudioHandler)
    }
    const unsubscribeAll = () => {
      window.removeEventListener('mousemove', startAudioHandler)
      window.removeEventListener('scroll', startAudioHandler)
      window.removeEventListener('keydown', startAudioHandler)
      window.removeEventListener('click', startAudioHandler)
      window.removeEventListener('touchstart', startAudioHandler)
      window.removeEventListener('wheel', startAudioHandler)
    }

    const startAudioHandler = () => {
      try {
        currentAudioRef.current.play().then(unsubscribeAll).catch(subscribeAll)
      } catch {
        unsubscribeAll()
      }
    }
    currentAudioRef.current.play().then(unsubscribeAll).catch(subscribeAll)

    const scrollHandler = e => {
      const newVolume = (
        0.01 * (e.deltaY < 0 ? 1 : -1) +
        currentAudioRef.current.volume
      ).toFixed(2)
      currentAudioRef.current.volume =
        newVolume > 1 ? 1 : newVolume < 0 ? 0 : newVolume
      localStorage.setItem(VOLUME_KEY, newVolume + '')
    }
    window.addEventListener('wheel', scrollHandler)

    prevAudioRef.current = audio
    localStorage.setItem(VOLUME_KEY, currentAudioRef.current.volume + '')
    return () => {
      currentAudioRef.current.removeEventListener('ended', audioEndHandler)
      window.removeEventListener('mousemove', startAudioHandler)
      window.removeEventListener('scroll', startAudioHandler)
      window.removeEventListener('keydown', startAudioHandler)
      window.removeEventListener('click', startAudioHandler)
      window.removeEventListener('touchstart', startAudioHandler)
      window.removeEventListener('wheel', scrollHandler)
      window.removeEventListener('wheel', startAudioHandler)
      currentAudioRef.current.pause()
    }
  }, [audio])

  return (
    <AudioContext.Provider
      value={{
        audio,
        setAudio,
      }}
    >
      {children}
    </AudioContext.Provider>
  )
}

export default AudioContextProvider

export const useAudio = () => useContext(AudioContext)
