import { forwardRef, useEffect, useRef, useState, useLayoutEffect } from 'react'
import { createPortal } from 'react-dom'
import './cards.css'
import { moveToCursorEventHandlerGenerator } from '../../Acryl/acrylHelpers'
import SwordIcon from '../../../icons/SwordIcon'
import ShieldIcon from '../../../icons/ShieldIcon'
import AdoptiveText from '../../AdoptiveText'

const CARD_DEFAULTS = {
  height: 0,
  width: 0,
  marginTop: 0,
  marginLeft: 0,
  marginBottom: 0,
}

const adoptiveStatsStyle = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  textAlign: 'center',
}

export const CardContent = forwardRef(
  ({ front = true, card, className, ...props }, ref) => (
    <div
      className={`card ${front ? '' : 'back'} ${className ?? ''}`}
      ref={ref}
      {...props}
    >
      {front && (
        <div className={`front ${card.rarity}`}>
          <div
            className="avatar"
            style={{
              backgroundImage: `url(/cards/${card.mask})`,
            }}
          />
          <div className="name">
            <AdoptiveText
              viewBox="0 0 500 250"
              fontSize="6rem"
              style={{
                textAlign: 'center',
              }}
            >
              {card.name}
            </AdoptiveText>
          </div>
          <div className="stat hp">
            <div>
              <AdoptiveText style={adoptiveStatsStyle} viewBox="0 0 20 20">
                {card.hp}
              </AdoptiveText>
            </div>
          </div>
          <div className="stat damage">
            <div>
              <AdoptiveText style={adoptiveStatsStyle} viewBox="0 0 20 20">
                {card.damage}
              </AdoptiveText>
            </div>
          </div>
          <div className="stat cost">
            <div>
              <AdoptiveText style={adoptiveStatsStyle} viewBox="0 0 20 20">
                {card.cost}
              </AdoptiveText>
            </div>
          </div>
          <div className="stat type">
            <div>
              {card.type === 'attack' ? (
                <SwordIcon width="100%" height="100%" className={card.type} />
              ) : (
                <ShieldIcon width="100%" height="100%" className={card.type} />
              )}
            </div>
          </div>
        </div>
      )}
      <div className="cover" data-target={props['data-target']} />
    </div>
  )
)

const Card = ({
  card,
  onRelease,
  front = true,
  parentRef = { current: document.body },
  isStatic,
  staticStyles: staticStylesProp = CARD_DEFAULTS,
  active,
  targetFilter,
  ...props
}) => {
  const [floating, setFloating] = useState(false)

  const [staticStyles, setStaticStyles] = useState(staticStylesProp)

  const isActiveRef = useRef(active)
  const cardRef = useRef(null)
  const floatingCardRef = useRef(null)
  const eventRef = useRef(null)

  const mouseDownHandler = e => {
    eventRef.current = e
    const elem = e.target
    elem.style.height = 0
    elem.style.width = 0
    elem.style.marginTop = 0
    elem.style.marginLeft = 0
    elem.style.marginBottom = 0
    elem.style.fontSize = 0
    setFloating(true)
  }

  useEffect(() => {
    isActiveRef.current = active
  }, [active])

  useEffect(() => {
    if (!floating) return
    const parent = parentRef.current

    const moveReactionHandler = e => {
      const target = e.target.dataset.target
      floatingCardRef.current.style.opacity =
        !target || (targetFilter && !targetFilter(target)) ? 0.3 : 0.8
    }

    const followHandler = moveToCursorEventHandlerGenerator(
      parent,
      floatingCardRef.current
    )
    if (eventRef.current) followHandler(eventRef.current)

    const finallyHandler = () => {
      parent.style.userSelect = null
      setFloating(false)
    }
    const mouseUpHandler = e => {
      const target = e.target.dataset.target
      target &&
        isActiveRef.current &&
        (targetFilter?.(target) ?? true) &&
        onRelease?.(card, target)
      finallyHandler(e)
    }

    parent.addEventListener('mousemove', followHandler)
    parent.addEventListener('mousemove', moveReactionHandler)
    parent.addEventListener('mouseleave', finallyHandler)
    parent.addEventListener('mouseup', mouseUpHandler)

    parent.style.userSelect = 'none'

    return () => {
      finallyHandler()
      parent.removeEventListener('mousemove', followHandler)
      parent.removeEventListener('mousemove', moveReactionHandler)
      parent.removeEventListener('mouseout', finallyHandler)
      parent.removeEventListener('mouseup', mouseUpHandler)
    }
  }, [floating, onRelease, targetFilter, parentRef.current])

  useEffect(() => {
    if (!isStatic) return
    const timeout = setTimeout(() => {
      setStaticStyles({})
    })
    return () => {
      clearTimeout(timeout)
    }
  }, [isStatic])

  const removeTimeoutRef = useRef(null)

  useLayoutEffect(() => {
    clearTimeout(removeTimeoutRef.current)
    return () => {
      if (!cardRef.current) return
      const elem = cardRef.current
      const clone = elem.cloneNode(true)
      clone.style.transition = '0.2s'
      const parent = cardRef.current.parentElement
      const position = [...parent.childNodes].indexOf(elem)
      removeTimeoutRef.current = setTimeout(() => {
        parent.insertBefore(clone, parent.children[position])
        setTimeout(() => {
          clone.style.height = 0
          clone.style.width = 0
          clone.style.fontSize = 0
          setTimeout(() => {
            clone.remove()
          }, 200)
        }, 30)
      })
    }
  }, [])

  return (
    <>
      {floating ? (
        createPortal(
          <CardContent
            className="floating"
            ref={floatingCardRef}
            card={card}
            front={front}
          />,
          document.body
        )
      ) : (
        <CardContent
          className={onRelease ? 'active' : ''}
          onMouseDown={onRelease ? mouseDownHandler : undefined}
          style={staticStyles}
          card={card}
          front={front}
          ref={cardRef}
          data-id={card.id}
          {...props}
        />
      )}
    </>
  )
}

export default Card
