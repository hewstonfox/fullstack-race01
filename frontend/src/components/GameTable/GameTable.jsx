import {
  useRef,
  useCallback,
  useEffect,
  useReducer,
  useLayoutEffect,
} from 'react'
import Deck from './Deck'
import Acryl from '../Acryl/Acryl'
import PlayerStats from './PlayerStats/PlayerStats'
import TurnButton from './TurnButton/TurnButton'
import Timer from './Timer/Timer'
import Board from './Board/Board'
import { useQueryClient } from 'react-query'
import './gameTable.css'
import { useSocket } from '../SocketContext/SocketContext'
import Loading from '../Loading'
import { DeferredVisibility } from '../DeferredVisibility'
import { useAudio } from '../AudioContext/AudioContext'

const initialState = {
  started: false,
  gameReady: false,
  isMyTurn: false,
  deck: 0,
  myBoard: [],
  opponentBoard: [],
  myCards: [],
  opponentCards: [],
  me: {
    login: '',
    avatarID: '',
    hp: 20,
    mana: 0,
  },
  opponent: {
    login: '',
    avatarID: '',
    hp: 20,
    mana: 0,
  },
  timeToNext: 0,
  currentMove: 0,
}

const gameReducer = (state, { type, payload }) => {
  console.log({
    type,
    payload,
  })
  switch (type) {
    case 'game':
      return {
        ...state,
        ...payload,
        gameReady: true,
      }
    default:
      return state
  }
}

const GameTable = () => {
  const queryClient = useQueryClient()

  const tableRef = useRef(null)
  const socketRef = useSocket()

  const { audio, setAudio } = useAudio()

  const [
    {
      started,
      gameReady,
      isMyTurn,
      myCards,
      opponentCards,
      myBoard,
      opponentBoard,
      me,
      opponent,
      timeToNext,
      currentMove,
    },
    dispatch,
  ] = useReducer(gameReducer, initialState)
  const dispatchRef = useRef(dispatch)

  useEffect(() => {
    dispatchRef.current = dispatch
  }, [dispatch])

  const endTurn = useCallback(() => {
    socketRef.current.emit('endTurn')
  }, [])

  const onAttack = useCallback((card, to) => {
    console.log('attack', { card, to })
    socketRef.current.emit('game:card:attack', { to: to + '', from: card.id })
  }, [])

  const onPut = useCallback(card => {
    console.log('put', card)
    socketRef.current.emit('game:card:put', card.id)
  }, [])

  useLayoutEffect(() => {
    const invalidateHandler = () => {
      console.log('invalidate')
      queryClient.invalidateQueries('user')
    }

    const updateHandler = (...args) => {
      dispatchRef.current?.(...args)
    }
    const connectHandler = () => {
      socketRef.current.emit('game:join')
    }

    socketRef.current.on('game:update', updateHandler)
    socketRef.current.on('game:join:fail', invalidateHandler)
    socketRef.current.on('connect', connectHandler)

    if (!socketRef.current.connected) socketRef.current.connect()

    return () => {
      socketRef.current.disconnect()
      socketRef.current.off('update', updateHandler)
      socketRef.current.off('game:join:fail', invalidateHandler)
      socketRef.current.off('connect', connectHandler)
      queryClient.invalidateQueries('user')
    }
  }, [])

  useEffect(() => {
    if (gameReady) socketRef.current.emit('game:ready')
  }, [gameReady])

  const isStarted = started

  useLayoutEffect(() => {
    if (!isStarted) {
      setAudio(new Audio('/loading.mp3'))
    }
    return () => {
      setAudio(null)
    }
  }, [isStarted])

  return (
    <Acryl
      contentClassName="game-table full-size"
      contentRef={tableRef}
      className="full-size"
    >
      <PlayerStats player={opponent} />
      <Deck cards={opponentCards} parentRef={tableRef} />
      <Board cards={opponentBoard} />
      <Timer time={timeToNext} onTimeout={isMyTurn ? endTurn : null} />
      <TurnButton onClick={endTurn} disabled={!isMyTurn} />
      <Board
        my
        cards={myBoard}
        parentRef={tableRef}
        onTurn={onAttack}
        active={isMyTurn}
        currentMove={currentMove}
      />
      <Deck
        my
        cards={myCards}
        parentRef={tableRef}
        onTurn={onPut}
        active={isMyTurn}
      />
      <PlayerStats my player={me} />
      <div style={{ height: '20px' }} />
      <DeferredVisibility show={!isStarted} timeoutMs={300}>
        <Acryl className={`loading ${isStarted && 'hiding'}`} disableCursor>
          <Loading />
        </Acryl>
      </DeferredVisibility>
    </Acryl>
  )
}

export default GameTable
