import { useCallback } from 'react'
import './board.css'
import Card from '../Cards/Card'
import { CARD_LIMIT } from '../../../constants'

const Board = ({ cards, my, parentRef, onTurn, active, currentMove }) => {
  const filter = useCallback(target => target !== 'board', [])

  return (
    <div
      className={`board ${my ? 'my' : 'opponent'}`}
      {...(my && cards.length < CARD_LIMIT && { 'data-target': 'board' })}
    >
      {cards.map(card => (
        <Card
          key={card.id}
          card={card}
          onRelease={my ? onTurn : undefined}
          className="board--card"
          parent={parentRef}
          active={active}
          targetFilter={filter}
          staticStyles={{}}
          isStatic={
            !my || !currentMove
              ? true
              : card.blockedToMove == null
              ? false
              : card.blockedToMove >= currentMove
          }
          {...(!my && { 'data-target': card.id })}
        />
      ))}
    </div>
  )
}

export default Board
