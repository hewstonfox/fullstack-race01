import { useState, useEffect, useRef } from 'react'
import './timer.css'
import Acryl from '../../Acryl/Acryl'

const Timer = ({ time, onTimeout, my }) => {
  const intervalRef = useRef(null)
  const [counter, setCounter] = useState(0)

  useEffect(() => {
    setCounter(time)
    intervalRef.current = setInterval(() => {
      setCounter(c => {
        if (c > 1) return c - 1
        clearInterval(intervalRef.current)
        onTimeout?.()
        return 0
      })
    }, 1_000)

    return () => {
      if (intervalRef.current) clearInterval(intervalRef.current)
    }
  }, [time, onTimeout])

  return (
    <Acryl
      className={`timer ${counter <= 5 ? 'danger' : ''} ${my ? 'my' : 'opponent'}`}
      disableCursor
      background="none"
      blendColor={my ? '#E5E5E5BF' : '#031622C0'}
      allClassName="timer--border"
      contentClassName="timer--content"
    >
      {counter} s.
    </Acryl>
  )
}

export default Timer
