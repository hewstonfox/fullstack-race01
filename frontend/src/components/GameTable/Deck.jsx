import './deck.css'
import { useCallback, useEffect, useRef, useState } from 'react'
import CardHand from '../../models/CardHand'
import Card from './Cards/Card'

const Deck = ({ cards = [], onTurn, parentRef, my = false, active }) => {
  const cardWrapperRef = useRef(null)
  const [cardsHand, setCardsHand] = useState(null)

  const alignCards = useCallback(() => {
    if (!cardsHand) return
    cardsHand.align()
    cardsHand.watch()
  }, [cardsHand])

  const filter = useCallback(target => target === 'board', [])

  useEffect(() => {
    setCardsHand(new CardHand(cardWrapperRef.current))
  }, [])

  useEffect(() => {
    alignCards()
  }, [alignCards, cards])

  return (
    <div className={`deck ${my ? 'my' : 'opponent'}`} ref={cardWrapperRef}>
      {cards.map(card => (
        <Card
          key={card.id}
          card={card}
          onRelease={onTurn}
          front={my}
          parent={parentRef}
          active={active}
          targetFilter={filter}
        />
      ))}
    </div>
  )
}

export default Deck
