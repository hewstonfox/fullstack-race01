import Acryl from '../../Acryl/Acryl'
import './playerStats.css'
import { USER_FILE_URL } from '../../../api/user'
import DeferredCounter from '../../DeferredCounter'
import avatarPlaceholder from '@/assets/avatarPlaceholder.jpg'

const PlayerStats = ({ player = null, my = false }) => {
  return (
    <Acryl
      className={`player-stats player-stats-border-radius ${
        my ? 'my' : 'opponent'
      }`}
      contentClassName="player-stats--content"
      allClassName="player-stats-border-radius"
      blendColor={my ? '#E5E5E5BF' : '#031622C0'}
      background="none"
      disableCursor
    >
      <DeferredCounter
        number={player.hp}
        className="player-stats--counter hp"
      />
      {!my && <div className="player-stats--login">{player.login}</div>}
      <div className="player-stats--avatar" data-target={player.login}>
        <img
          src={
            player.avatarID
              ? `${USER_FILE_URL}/${player.avatarID}`
              : avatarPlaceholder
          }
          alt="avatar"
        />
      </div>
      <DeferredCounter
        number={player.mana}
        className="player-stats--counter mana"
      />
    </Acryl>
  )
}

export default PlayerStats
