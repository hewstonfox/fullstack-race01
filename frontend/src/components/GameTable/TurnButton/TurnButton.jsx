import './turnButton.css'

const TurnButton = props => (
  <button {...props} className="turn-button">
    END TURN
  </button>
)

export default TurnButton
