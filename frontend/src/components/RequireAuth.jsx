import { useUser } from '../queries'
import { useLocation, Navigate } from 'react-router-dom'
import Loading from './Loading'

const RequireAuth = ({ children }) => {
  const { data, isLoading } = useUser()
  const location = useLocation()

  return isLoading ? <Loading /> : data ? children : <Navigate to="/auth/login" from={location} />
}

export default RequireAuth
