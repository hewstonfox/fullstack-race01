import { useUser } from '../queries'
import { useLocation, Navigate } from 'react-router-dom'
import Loading from './Loading'

const NotAuthorizedOnly = ({ children }) => {
  const { data, isLoading } = useUser()
  const location = useLocation()

  return isLoading ? <Loading /> : !data ? children : <Navigate to="/" from={location} />
}

export default NotAuthorizedOnly
