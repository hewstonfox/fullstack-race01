import { useEffect, useRef, useState } from 'react'

const DeferredCounter = ({ number, timeout, ...props }) => {
  const intervalRef = useRef(null)
  const [counter, setCounter] = useState(0)

  useEffect(() => {
    intervalRef.current = setInterval(
      () =>
        setCounter(prevCounter => {
          if (prevCounter === number) {
            clearInterval(intervalRef.current)
            return prevCounter
          }
          return prevCounter > number ? prevCounter - 1 : prevCounter + 1
        }),
      100
    )
    return () => {
      clearInterval(intervalRef.current)
    }
  }, [number])

  return <div {...props}>{counter}</div>
}

export default DeferredCounter
