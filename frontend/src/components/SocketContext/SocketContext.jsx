import { createContext, useRef, useEffect, useContext } from 'react'
import { getGameSocket } from '../../api/socket/gameSocket'

export const SocketContext = createContext(null)

export const SocketProvider = ({ children }) => {
  const socketRef = useRef(null)

  useEffect(() => {
    socketRef.current = getGameSocket()
  }, [])

  return <SocketContext.Provider value={socketRef}>{children}</SocketContext.Provider>
}

export const useSocket = () => {
  return useContext(SocketContext)
}
