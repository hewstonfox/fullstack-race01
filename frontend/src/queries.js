import { useQuery } from 'react-query'
import { getUser } from './api/user'

export const useUser = () => useQuery('user', getUser)
