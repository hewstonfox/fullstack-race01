export const USER_STATUS = {
  IDLE: 'idle',
  PLAYING: 'playing',
  SEARCHING: 'searching',
}
export const CARD_LIMIT = 8

export const COOKIE_KEY = 'TYAN_BATTLE_USER_TOKEN'
