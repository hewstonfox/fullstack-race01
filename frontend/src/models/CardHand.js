import { debounce } from '../utils/timing'

export class CardHand {
  static MIN_CARDS = 5
  static CARD_SIDES_PROPORTION = 1.6

  constructor(parentNode, cardSelector = null) {
    this.parentNode = parentNode
    this.cardSelector = cardSelector
  }

  watch() {
    this.mutationObserver?.disconnect()
    this.mutationObserver = new MutationObserver(() => this.align())
    this.mutationObserver.observe(this.parentNode, { childList: true })

    this.resizeObserver?.disconnect()
    this.resizeObserver = new ResizeObserver(debounce(() => this.align(), 200))
    this.resizeObserver.observe(this.parentNode)
  }

  collectCurrentCards() {
    if (!this.cardSelector) return [...this.parentNode.children]
    return [...this.parentNode.querySelectorAll(this.cardSelector)]
  }

  align() {
    const cards = this.collectCurrentCards()
    const realCount = cards.length
    const count = this.constructor.MIN_CARDS > realCount ? this.constructor.MIN_CARDS : realCount
    const halfCount = count / 2
    const handWidth = parseFloat(getComputedStyle(this.parentNode).width)
    const cardWidth = handWidth / count
    const cardHeight = this.constructor.CARD_SIDES_PROPORTION * cardWidth
    const overlap = -(cardWidth / 3)
    const positionGap = (count - realCount) / 2
    const topGap = cardHeight / 10
    const rotationGap = 30 / realCount

    for (const _i in cards) {
      const i = +_i
      const card = cards[i]
      const emulatedPosition = i + positionGap
      const heightIndex = emulatedPosition < halfCount ? emulatedPosition : count - emulatedPosition - 1
      const rotationIndex = (~~halfCount - heightIndex) * (heightIndex === emulatedPosition ? -1 : 1)

      card.style.width = cardWidth + 'px'
      card.style.height = cardHeight + 'px'

      card.style.marginLeft = i === 0 ? overlap + 'px' : 0
      card.style.marginRight = overlap + 'px'

      card.style.marginTop = -heightIndex * topGap + 'px'

      card.style.transform = `rotate(${rotationIndex * rotationGap}deg)`
    }
  }
}

export default CardHand
