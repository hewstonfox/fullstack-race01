import React from 'react'
import ReactDOM from 'react-dom/client'
import { QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import { BrowserRouter } from 'react-router-dom'
import './style/index.css'
import AppRoutes from './AppRoutes'
import { SocketProvider } from './components/SocketContext/SocketContext'
import AudioContextProvider from './components/AudioContext/AudioContext'
import { queryClient } from './queryClient'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <SocketProvider>
        <AudioContextProvider>
          <BrowserRouter>
            <AppRoutes />
          </BrowserRouter>
        </AudioContextProvider>
      </SocketProvider>
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  </React.StrictMode>
)
