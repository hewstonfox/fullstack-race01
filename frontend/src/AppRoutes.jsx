import * as React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import LoginPage from './pages/auth/LoginPage'
import RegisterPage from './pages/auth/RegisterPage'
import MainPage from './pages/main/MainPage'
import RequireAuth from './components/RequireAuth'
import NotAuthorizedOnly from './components/NotAuthorizedOnly'
import AuthLayout from './pages/auth/AuthLayout'
import MePage from './pages/main/MePage'
import GamePage from './pages/main/GamePage'

const authorizedRoute = (path, element) => <Route path={path} element={<RequireAuth>{element}</RequireAuth>} />

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/">
        <Route
          path="auth/*"
          element={
            <NotAuthorizedOnly>
              <Routes>
                <Route path="/" element={<AuthLayout />}>
                  <Route path="login" element={<LoginPage />} />
                  <Route path="registration" element={<RegisterPage />} />
                  <Route index element={<Navigate to="login" />} />
                  <Route path="*" element={<Navigate to="login" />} />
                </Route>
              </Routes>
            </NotAuthorizedOnly>
          }
        />
        {authorizedRoute('/', <MainPage />)}
        {authorizedRoute('/me', <MePage />)}
        {authorizedRoute('/game', <GamePage />)}
      </Route>
    </Routes>
  )
}

export default AppRoutes
