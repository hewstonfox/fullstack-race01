import axios from 'axios'
import { COOKIE_KEY } from '../constants'

export const API_BASE_URL = import.meta.env.VITE_API_URL

export const api = axios.create({
  baseURL: API_BASE_URL,
  withCredentials: true,
})

export const reqData = async axiosRequest => {
  try {
    const res = await axiosRequest
    return res.data
  } catch {
    return null
  }
}

export const getToken = () =>
  document.cookie.replace(new RegExp(`(?:(?:^|.*;\\s*)${COOKIE_KEY}\\s*\\=\\s*([^;]*).*$)|^.*$`), '$1')
