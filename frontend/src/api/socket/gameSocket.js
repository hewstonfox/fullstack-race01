import { io } from 'socket.io-client'
import { getToken } from '..'
import { queryClient } from '../../queryClient'

export const getGameSocket = () => {
  const socket = io(import.meta.env.VITE_GAME_SOCKET_URL, {
    auth: cb => cb({ token: getToken() }),
    autoConnect: false,
  })

  socket.on('connect_error', err => {
    console.log('SOCKET: connect error', err)
    setTimeout(() => {
      socket.connect()
    }, 1000)
  })

  socket.io.on('reconnect_attempt', attempt => {
    console.log('SOCKET IO: reconnect attempt', attempt)
  })

  socket.io.on('reconnect', () => {
    console.log('SOCKET IO: reconnect')
  })

  socket.on('disconnect', reason => {
    setTimeout(() => {
      console.log('disconnect invalidate')
      queryClient.invalidateQueries('user')
    }, 1000)
    console.log('SOCKET: disconnected', { reason })
  })

  socket.on('connect', () => {
    setTimeout(() => {
      console.log('connect invalidate')
      queryClient.invalidateQueries('user')
    })

    const engine = socket.io.engine

    console.log('SOCKET: connected', socket.id)

    engine.once('upgrade', () => {
      console.log('SOCKET ENGINE: upgraded to', engine.transport.name)
    })

    engine.on('packet', packet => {
      console.log('SOCKET ENGINE: packet received', packet)
    })

    engine.on('packetCreate', packet => {
      console.log('SOCKET ENGINE: packet sent', packet)
    })
  })

  return socket
}
