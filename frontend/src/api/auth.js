import { api } from './index'

export const AUTH_ENDPOINT = '/auth'

export const login = ({ login, password }) => api.post(`${AUTH_ENDPOINT}/login`, { login, password })
export const register = ({ login, password }) => api.post(`${AUTH_ENDPOINT}/register`, { login, password })
export const logout = () => api.post(`${AUTH_ENDPOINT}/logout`)
