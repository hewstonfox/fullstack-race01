import { api, API_BASE_URL, reqData } from './index'

export const USER_ENDPOINT = '/user'

export const USER_AVATAR_URL = `${API_BASE_URL}${USER_ENDPOINT}/avatar`

export const USER_FILE_URL = `${API_BASE_URL}${USER_ENDPOINT}/file`

export const setMyAvatar = file => {
  const form = new FormData()
  form.append('avatar', file)
  return api.post(`${USER_ENDPOINT}/avatar`, form)
}
export const getUser = () => reqData(api.get(USER_ENDPOINT))

export const updateUserPassword = (currentPassword, newPassword) =>
  api.patch(`${USER_ENDPOINT}/password`, { currentPassword, newPassword })

export const updateUser = data => reqData(api.patch(USER_ENDPOINT, data))
