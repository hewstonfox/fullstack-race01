export const file2dataUrl = file =>
  new Promise(res => {
    const reader = new FileReader()
    reader.onloadend = () => res(reader.result)
    reader.readAsDataURL(file)
  })

export const dataUrl2Blob = url => {
  return fetch(url).then(r => r.blob())
}
