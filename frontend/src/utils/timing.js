export const progressedInterval = (cb, timeout, cooldown = 100) => {
  let progress = 0
  const frameProgress = cooldown / timeout
  const interval = setInterval(() => {
    const res = cb(progress > 1 ? 1 : progress, frameProgress)
    if (progress >= 1 || res === true) clearInterval(interval)
    progress += frameProgress
  }, cooldown)
  return interval
}

export function debounce(fn, wait = 0) {
  let timeout
  return function (...args) {
    clearTimeout(timeout)
    timeout = setTimeout(() => fn.call(this, ...args), wait)
  }
}
